#!/bin/bash
PROG=$(basename $0)
# required packages
dnf install python3 python3-devel certbot python3-certbot-nginx gcc kernel-headers-$(uname -r) gzip expect -y

### Just the security updates wanted during the course
# if needed, update before adding software
# dnf update --exclude falcon-sensor # had troubles updating not excluding this
sed -i 's/yum -y/yum --security -y/g' /etc/cron.d/yum_autoupdate
# Set up the nightly reboot
echo "30 04 * * * /sbin/shutdown -r" | tee -a /var/spool/cron/root

### INSTRUCTOR ACCOUNTS
useradd -d /home/mqmadm -m mqmadm
echo "Cr8zy1fraUzerP" | passwd mqmadm --stdin
echo "mqmadm ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
mkdir -m 0600 /home/mqmadm/.ssh
chown -R mqmadm:mqmadm /home/mqmadm/.ssh
### ryanburk for the instructor
useradd -d /home/ryanburk -m ryanburk
echo "RyAn9Kn0wsD8ta" | passwd ryanburk --stdin
echo "ryanburk ALL=(ALL) ALL" >> /etc/sudoers
mkdir -m 0600 /home/ryanburk/.ssh
chown -R ryanburk:ryanburk /home/ryanburk/.ssh
### The ssh key (work in progress)

### firewall stuff
systemctl enable --now firewalld
firewall-cmd --permanent --zone=public --add-service=https --add-service=http --add-service=mysql 
firewall-cmd --permanent --zone=public --add-port=8888/tcp
firewall-cmd  --reload

### MariaDB
groupadd mysql
useradd -g mysql mysql
### MariaDB 10.11
cd /tmp/${INSTALL}
curl -LsS -O https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
bash mariadb_repo_setup --mariadb-server-version=10.11
dnf install boost-program-options -y
dnf module reset mariadb -y
dnf install MariaDB-server MariaDB-client MariaDB-backup -y

# customized setting
cat <<EOF >> /etc/my.cnf
[mariadb]
general_log = 0 # DEFAULT
innodb-buffer-pool-size = 2500M
EOF
systemctl enable --now mariadb


### nginx 
dnf remove nginx -y
dnf module reset nginx -y
dnf module enable nginx -y
dnf install nginx -y
sed -i -e 's/  listen       \[\:/ # listen       \[\:/g' -e 's/server_name  _;/server_name  '$(hostname)';/' /etc/nginx/nginx.conf
systemctl start --now nginx.service 

### Now we install anaconda and jupyter
echo "*** $PROG: Installing anaconda";
time (
    wget https://repo.anaconda.com/archive/Anaconda3-2024.06-1-Linux-x86_64.sh;
    bash Anaconda3-2024.06-1-Linux-x86_64.sh -b -f -p /opt/conda;
    # /opt/conda/bin/conda update -y -n base -c defaults conda
    /opt/conda/bin/pip install jupyter-c-kernel;
    /opt/conda/bin/conda install -y -c anaconda pymysql;
    /opt/conda/bin/pip install ipython-sql==0.5.0;
)