#!/bin/bash
PROG=$(basename $0)
export INSTALL=$(basename $(pwd -P))
export FUQUA=/usr/local/share/fuqua
mkdir -m 700 $FUQUA
export student_pw=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
echo $student_pw > $FUQUA/stud_pw.txt
echo $student_pw > $FUQUA/fuqua-mqm-token.txt
hostname >> $FUQUA/fuqua-mqm-token.txt

# remote root login -- replace key every year
cat mqm/annual/source_files/mqmkey.pub >> /root/.ssh/authorized_keys

## Do the install here
## Part 1 build up the basics
echo "*** $PROG: Install nginx, mariadb, anaconda, and jupyter"
./kickoff_base_sw.sh | tee /tmp/rapid_image_status.txt
if [[ ${PIPESTATUS[0]} != 0 ]]; then
    echo "Base installation failed"
    echo "fail" > /tmp/rapid_image_complete
    exit ${PIPESTATUS[0]}
fi

## Part 2 Add the databases, database user, etc.
echo "*** $PROG: Configure databases"
./kickoff_dbs.sh | tee -a /tmp/rapid_image_status.txt
if [[ ${PIPESTATUS[0]} != 0 ]]; then
    echo "Database installation failed"
    echo "fail" > /tmp/rapid_image_complete
    exit ${PIPESTATUS[0]}
fi

## Part 3 Add the Jupyter (jovyan) part
echo "*** $PROG: Configure jupyter notebook"
./kickoff_jupyter.sh | tee -a /tmp/rapid_image_status.txt
if [[ ${PIPESTATUS[0]} != 0 ]]; then
    echo "Jupyter installation failed"
    echo "fail" > /tmp/rapid_image_complete
    exit ${PIPESTATUS[0]}
fi

echo "Virtual Machine installation Succeeded"
echo "success" > /tmp/rapid_image_complete
if [ -n "$INSTALL" -a -d "/tmp/${INSTALL}" ]; then
    exec sh -c 'rm -rf /tmp/${INSTALL}'
fi
