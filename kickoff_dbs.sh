#!/bin/bash
PROG=$(basename $0)
ROOT_PW=tDSKY3tnA3LOL
declare -a DBList=(	airline_ontime \
                    dognitiondb \
                    lahman2021 \
                    lecture2 \
                    lecture3 \
                    pir \
                    sanford \
                    simpsons \
                    yelp \
)

# create student account
mysql -u root <<EOF
create user 'student'@'%' identified by '$student_pw' with max_statement_time 1800;
create user 'student'@'localhost' identified by '$student_pw' with max_statement_time 1800;
EOF

ln -s /tmp/${INSTALL}/mqm /var/lib/mysql/mqm
cp /etc/my.cnf{,-SAVE}
cp mqm/annual/source_files/my.cnf-LOAD /etc/my.cnf
systemctl restart mariadb
my_print_defaults --mysqld

for DB in ${DBList[@]}; do
    echo "*** $PROG: prepare ${DB} files"
    (cd mqm/annual/source_files/${DB}; pigz -d *)
    mysql -u root <<EOF
drop database if exists ${DB};
create database ${DB};
EOF
    echo "*** $PROG: build ${DB} database"
    time mysql -u root ${DB} < mqm/annual/source_files/build.${DB}.sql
done

mv /etc/my.cnf{-SAVE,}
systemctl restart mariadb
my_print_defaults --mysqld

# Setup student account access
mysql -u root <<EOF
use 'simpsons';
GRANT SELECT ON simpsons.* TO 'student'@'localhost';
GRANT SELECT ON simpsons.* TO 'student'@'%';
use 'lecture3';
grant select on lecture3.* to 'student'@'localhost';
grant select on lecture3.* to 'student'@'%'; 
use 'sanford';
grant select on sanford.* to 'student'@'localhost';
grant select on sanford.* to 'student'@'%';
use 'lecture2';
grant select on lecture2.* to 'student'@'localhost';
grant select on lecture2.* to 'student'@'%';
use 'airline_ontime';
grant select on airline_ontime.* to 'student'@'localhost';
grant select on airline_ontime.* to 'student'@'%';
use 'yelp';
grant select on yelp.* to 'student'@'localhost';
grant select on yelp.* to 'student'@'%';
use 'pir';
grant select on pir.* to 'student'@'localhost';
grant select on pir.* to 'student'@'%';
EOF

# setup admin access for root user
mysql -u root <<EOF
ALTER USER 'root'@'localhost' IDENTIFIED BY '${ROOT_PW}';
commit;
EOF

cat <<EOF > /root/.my.cnf
[client]
[mysql]
# ROOT login
user=root
password="${ROOT_PW}"
database=mysql
EOF
