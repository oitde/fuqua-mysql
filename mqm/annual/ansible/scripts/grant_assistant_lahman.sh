#!/bin/bash
# File: grant_assistant_lahman.sh
# Written by: Beth Good 09/01/2020
# Script to grant the assistant user permission on lahman2021 database.

# Source environment for root and set variables
$HOME/.bash_profile

export DB_ADMIN=/usr/local/bin/fuqua


# Assign role by running the sql script previously created.
cd $DB_ADMIN
mysql -u root < assistant_perms_lahman.sql

echo "We assigned the lahman2021 DB to the "assistant" for the mqm class" 

