#!/bin/bash

#host=$filename

#echo $host

#cat /usr/local/bin/fuqua/mqm/2023/hostfiles/fuqua-mqm-*-token.txt > master_hosts.csv

cat /dev/null > canvas_db_info.csv

for i in `ls /usr/local/bin/fuqua/mqm/2023/hostfiles/fuqua-mqm-*-token.txt | xargs -n1 basename`
do
netid=$(echo $i | awk -F '-' {'print $3'})
host=$(echo $i | sed s/-token.txt//)
token=$(cat $i)
echo $netid,$token,$host.fuqua.duke.edu >> canvas_db_info.csv
done
