#!/bin/bash
### Stop the jupyter service and
### Clean up the jupyter notebook config and
### Create a new SSL Cert for the cname and
### Stop/disable nginx and
### Start jupyter again
###
grep `hostname` /usr/local/bin/fuqua_assigned_nopw.csv | cut -f3 -d',' > /usr/local/bin/cname.txt
thiscname="`cat /usr/local/bin/cname.txt`"
systemctl stop jupyter.service 
natok="`cat /usr/local/share/stud_pw.txt`"
thisip="`hostname -i`"
cat <<EOF > /var/tmp/jupyter_notebook_config_tmp.py
# Configuration file for jupyter-notebook." 
c = get_config()  #noqa
import platform
c.NotebookApp.custom_display_url = platform.node() + ":8888"
c.NotebookApp.open_browser = False
c.NotebookApp.ip = '*'
c.NotebookApp.port = 8888
c.NotebookApp.token = '${natok}'
c.ConnectionFileMixin.ip = '${thisip}'
c.KernelManager.ip = '${thisip}'
c.NotebookApp.certfile = '/etc/pki/tls/fullchain1.pem'
c.NotebookApp.keyfile = '/etc/pki/tls/private/privkey1.pem'
EOF
chmod 664 /var/tmp/jupyter_notebook_config_tmp.py
chown jovyan /var/tmp/jupyter_notebook_config_tmp.py
cp -p /var/tmp/jupyter_notebook_config_tmp.py /home/jovyan/.jupyter/jupyter_notebook_config.py
### Below assumes nginx is running
certbot --server https://locksmith.oit.duke.edu/acme/v2/directory --agree-tos --email fuqua-security@duke.edu --no-eff-email -d ${thiscname} --nginx -n
cp -p /etc/letsencrypt/live/${thiscname}/fullchain.pem /etc/pki/tls/fullchain1.pem
cp -p /etc/letsencrypt/live/${thiscname}/privkey.pem /etc/pki/tls/private/privkey1.pem
chown jovyan /etc/pki/tls/fullchain1.pem
chown jovyan /etc/pki/tls/private/privkey1.pem
chmod 660 /etc/pki/tls/private/privkey1.pem
systemctl disable --now nginx.service 
systemctl start jupyter.service 
systemctl -l status jupyter.service 

