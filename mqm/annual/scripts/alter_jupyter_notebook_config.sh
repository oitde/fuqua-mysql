#!/bin/bash
natok="`cat /usr/local/bin/fuqua/stud_pw.txt`"
thisip="`hostname -i`"
cat <<EOF > /var/tmp/jupyter_notebook_config_tmp.py
# Configuration file for jupyter-notebook." 
c = get_config()  #noqa
import platform
c.NotebookApp.custom_display_url = platform.node() + ":8888"
c.NotebookApp.open_browser = False
c.NotebookApp.ip = '*'
c.NotebookApp.port = 8888
c.NotebookApp.token = '${natok}'
c.ConnectionFileMixin.ip = '${thisip}'
c.KernelManager.ip = '${thisip}'
c.NotebookApp.certfile = '/etc/pki/tls/fullchain1.pem'
c.NotebookApp.keyfile = '/etc/pki/tls/private/privkey1.pem'
EOF
chmod 664 /var/tmp/jupyter_notebook_config_tmp.py
chown jovyan /var/tmp/jupyter_notebook_config_tmp.py
cp -p /var/tmp/jupyter_notebook_config_tmp.py /home/jovyan/.jupyter/jupyter_notebook_config.py
