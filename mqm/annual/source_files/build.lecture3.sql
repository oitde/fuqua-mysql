
DROP TABLE IF EXISTS `CTE_Examp`;


CREATE TABLE `CTE_Examp` (
  `date` date DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/lecture3/CTE_Examp" REPLACE INTO TABLE CTE_Examp;


DROP TABLE IF EXISTS `Dogs`;


CREATE TABLE `Dogs` (
  `ownerid` int(11) DEFAULT NULL,
  `dogname` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/lecture3/Dogs" REPLACE INTO TABLE Dogs;


DROP TABLE IF EXISTS `Owners`;


CREATE TABLE `Owners` (
  `ownerid` int(11) DEFAULT NULL,
  `ownername` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/lecture3/Owners" REPLACE INTO TABLE Owners;
