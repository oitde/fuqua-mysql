
DROP TABLE IF EXISTS `characters`;


CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `normalized_name` varchar(100) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_characters_on_name` (`name`) USING BTREE,
  UNIQUE KEY `index_characters_on_normalized_name` (`normalized_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/simpsons/characters" REPLACE INTO TABLE characters;


DROP TABLE IF EXISTS `episodes`;


CREATE TABLE `episodes` (
  `id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `original_air_date` date DEFAULT NULL,
  `production_code` varchar(100) DEFAULT NULL,
  `season` int(11) DEFAULT NULL,
  `number_in_season` int(11) DEFAULT NULL,
  `number_in_series` int(11) DEFAULT NULL,
  `us_viewers_in_millions` double DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `imdb_rating` double DEFAULT NULL,
  `imdb_votes` int(11) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_episodes_on_production_code` (`production_code`) USING BTREE,
  UNIQUE KEY `index_episodes_on_season_and_number_in_season` (`season`,`number_in_season`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/simpsons/episodes" REPLACE INTO TABLE episodes;


DROP TABLE IF EXISTS `locations`;


CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `normalized_name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_locations_on_name` (`name`) USING BTREE,
  UNIQUE KEY `index_locations_on_normalized_name` (`normalized_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/simpsons/locations" REPLACE INTO TABLE locations;


DROP TABLE IF EXISTS `script_lines`;


CREATE TABLE `script_lines` (
  `id` int(11) NOT NULL,
  `episode_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `raw_text` mediumtext DEFAULT NULL,
  `timestamp_in_ms` int(11) DEFAULT NULL,
  `speaking_line` char(1) DEFAULT NULL,
  `character_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `raw_character_text` varchar(100) DEFAULT NULL,
  `raw_location_text` varchar(100) DEFAULT NULL,
  `spoken_words` mediumtext DEFAULT NULL,
  `normalized_text` mediumtext DEFAULT NULL,
  `word_count` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_script_lines_on_episode_id_and_number` (`episode_id`,`number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/simpsons/script_lines" REPLACE INTO TABLE script_lines;

