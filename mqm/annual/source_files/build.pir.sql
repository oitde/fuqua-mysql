
DROP TABLE IF EXISTS `five_ep`;


CREATE TABLE `five_ep` (
  `id` smallint(6) NOT NULL COMMENT 'unique event identifier',
  `date` date DEFAULT NULL COMMENT 'original airing date of show (there is only one show on each date)',
  `name` varchar(30) DEFAULT NULL COMMENT 'first name of contestant',
  `eventAmount` double(10,2) DEFAULT NULL COMMENT 'bid/spin amount (NULL for all pricing games)',
  `eventOrder` smallint(6) DEFAULT NULL COMMENT 'turn order for a specific eventTypeCounter in a specific show',
  `eventType` varchar(30) DEFAULT NULL COMMENT 'one of the four types of show events: bidders row, pricing game, big wheel, or showcase showdown',
  `eventTypeCounter` smallint(6) DEFAULT NULL COMMENT 'equals 1 for the first event (of each type) in a specific show and iterates sequentially as the show progresses',
  `prize` varchar(100) DEFAULT NULL COMMENT 'short description of prize(s) associated with the event',
  `price` mediumint(9) DEFAULT NULL COMMENT 'value of prize in USD',
  `game` varchar(30) DEFAULT NULL COMMENT 'name of pricing game',
  `numAttempts` smallint(6) DEFAULT NULL COMMENT 'number of attempts the contestant had to win the pricing game prize',
  `win` double(4,1) DEFAULT NULL COMMENT 'equals 0 if contestant loses a bidders row/pricing game/showcase showdown event, equals 1 if contestant wins a bidders row/pricing game/showcase showdown event, equals 0.5 if contestant wins less than the full prize for a pricing game event',
  `guessPrizePriceGame` smallint(6) DEFAULT NULL COMMENT 'equals 1 if a pricing game requires the contestant to guess the prize price and 0 if not',
  `spinNum` smallint(6) DEFAULT NULL COMMENT 'spin counter for each contestant in a specific big wheel event',
  `progressive` smallint(6) DEFAULT NULL COMMENT 'equals 1 if the pricing game offers progressively larger prize values up to the full prize amount',
  `progressivePrizeIfDifferent` varchar(30) DEFAULT NULL COMMENT 'prize description if a contestant wins a progressive pricing game prize that is not the full prize',
  `consolation` smallint(6) DEFAULT NULL COMMENT 'equals 1 if the pricing game potentially offers a consolation prize (or prizes) in the event the contestant does not win the full prize',
  `consolationPrizeIfWon` varchar(50) DEFAULT NULL COMMENT 'prize description if a contestant wins a consolation prize instead of the full prize',
  `stopEarly` smallint(6) DEFAULT NULL COMMENT 'equals 0 if the contestant was given the option to stop early in the pricing game and chose not to and 1 if the contestant did choose to stop early',
  `wouldHaveWon` smallint(6) DEFAULT NULL COMMENT 'equals 0 if the contestant stops early and it is revealed that they would not have won the pricing game and 1 if the contestant would have won',
  `firstShowcaseDecision` varchar(10) DEFAULT NULL COMMENT 'equals bid if the first contestant in the showcase showdown decides to bid on the first showcase and equals pass if the first contestant decides to pass the first showcase to the second contestant',
  `description` varchar(10000) DEFAULT NULL COMMENT 'long description of the prize(s)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/pir/five_ep" REPLACE INTO TABLE five_ep;

