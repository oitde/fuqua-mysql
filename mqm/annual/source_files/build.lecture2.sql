
DROP TABLE IF EXISTS `Reviews`;


CREATE TABLE `Reviews` (
  `ReviewID` int(11) NOT NULL,
  `Profile` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `Genre` varchar(45) DEFAULT NULL,
  `Title` varchar(45) DEFAULT NULL,
  `Rating` int(11) DEFAULT NULL,
  `RateDate` date DEFAULT NULL,
  PRIMARY KEY (`ReviewID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/lecture2/Reviews" REPLACE INTO TABLE Reviews;
