CREATE TABLE `complete_tests` (
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_guid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dog_guid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `idx_complete_tests_user_guid` (`user_guid`),
  KEY `idx_complete_tests_dog_guid` (`dog_guid`),
  KEY `createdat_ind` (`created_at`),
  KEY `testname_ind` (`test_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

load data infile "mqm/annual/source_files/dognitiondb/complete_tests" replace into table complete_tests;

CREATE TABLE `dogs` (
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `breed` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `dog_fixed` tinyint(1) DEFAULT NULL,
  `dna_tested` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dimension` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `exclude` tinyint(1) DEFAULT NULL,
  `breed_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `breed_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dog_guid` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_guid` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_tests_completed` varchar(255) DEFAULT NULL,
  `mean_iti_days` varchar(255) DEFAULT NULL,
  `mean_iti_minutes` varchar(255) DEFAULT NULL,
  `median_iti_days` varchar(255) DEFAULT NULL,
  `median_iti_minutes` varchar(255) DEFAULT NULL,
  `time_diff_between_first_and_last_game_days` varchar(255) DEFAULT NULL,
  `time_diff_between_first_and_last_game_minutes` varchar(255) DEFAULT NULL,
  KEY `unq_dogs_guid` (`dog_guid`(36)),
  KEY `idx_dogs_user_guid` (`user_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

load data infile "mqm/annual/source_files/dognitiondb/dogs" replace into table dogs;

CREATE TABLE `exam_answers` (
  `script_detail_id` int(11) DEFAULT NULL,
  `subcategory_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `step_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `loop_number` int(11) DEFAULT NULL,
  `dog_guid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `idx_exams_dog_guid` (`dog_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

load data infile "mqm/annual/source_files/dognitiondb/exam_answers" replace into table exam_answers;

CREATE TABLE `reviews` (
  `rating` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_guid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dog_guid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcategory_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `test_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `idx_reviews_user_guid` (`user_guid`),
  KEY `idx_reviews_dog_guid` (`dog_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

load data infile "mqm/annual/source_files/dognitiondb/reviews" replace into table reviews;

CREATE TABLE `site_activities` (
  `activity_type` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `membership_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `script_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_guid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `script_detail_id` int(11) DEFAULT NULL,
  `test_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dog_guid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `index_site_activities_on_activity_type` (`activity_type`),
  KEY `idx_site_activities_user_guid` (`user_guid`),
  KEY `idx_site_activities_dog_guid` (`dog_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

load data infile "mqm/annual/source_files/dognitiondb/site_activities" replace into table site_activities;

CREATE TABLE `users` (
  `sign_in_count` int(11) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `max_dogs` int(11) DEFAULT 0,
  `membership_id` int(11) DEFAULT NULL,
  `subscribed` tinyint(1) DEFAULT 0,
  `exclude` tinyint(1) DEFAULT NULL,
  `free_start_user` tinyint(1) DEFAULT NULL,
  `last_active_at` datetime DEFAULT NULL,
  `membership_type` int(11) DEFAULT NULL,
  `user_guid` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `utc_correction` varchar(255) DEFAULT NULL,
  KEY `unq_users_guid` (`user_guid`(36))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

load data infile "mqm/annual/source_files/dognitiondb/users" replace into table users;
