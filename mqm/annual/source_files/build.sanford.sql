
DROP TABLE IF EXISTS `health`;


CREATE TABLE `health` (
  `id` int(11) NOT NULL,
  `sex` varchar(45) DEFAULT NULL,
  `age` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `hypertension` int(11) DEFAULT NULL,
  `vasc_disease` int(11) DEFAULT NULL,
  `payor` varchar(45) DEFAULT NULL,
  `diabetes` int(11) DEFAULT NULL,
  `a1c` decimal(12,1) DEFAULT NULL,
  `bmi` decimal(12,2) DEFAULT NULL,
  `visits_sched` varchar(45) DEFAULT NULL,
  `visits_miss` varchar(45) DEFAULT NULL,
  `dbp` int(11) DEFAULT NULL,
  `sbp` int(11) DEFAULT NULL,
  `smoke` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/sanford/health" REPLACE INTO TABLE health;
