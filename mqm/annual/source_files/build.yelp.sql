
DROP TABLE IF EXISTS `business`;


CREATE TABLE `business` (
  `business_id` varchar(30) NOT NULL,
  `name` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` text DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `stars` float DEFAULT NULL,
  `review_count` smallint(5) unsigned DEFAULT NULL,
  `is_open` tinyint(1) DEFAULT NULL,
  `categories` text DEFAULT NULL,
  `priceRange2` varchar(5) DEFAULT NULL,
  `hoursMon` varchar(12) DEFAULT NULL,
  `hoursTue` varchar(12) DEFAULT NULL,
  `hoursWed` varchar(12) DEFAULT NULL,
  `hoursThu` varchar(12) DEFAULT NULL,
  `hoursFri` varchar(12) DEFAULT NULL,
  `hoursSat` varchar(12) DEFAULT NULL,
  `hoursSun` varchar(12) DEFAULT NULL,
  `takeout` varchar(12) DEFAULT NULL,
  `delivery` varchar(12) DEFAULT NULL,
  `driveThru` varchar(12) DEFAULT NULL,
  `agesAllowed` varchar(10) DEFAULT NULL,
  `alcohol` varchar(30) DEFAULT NULL,
  `attire` varchar(15) DEFAULT NULL,
  `bikeParking` varchar(12) DEFAULT NULL,
  `byob` varchar(12) DEFAULT NULL,
  `byobCorkage` varchar(15) DEFAULT NULL,
  `caters` varchar(12) DEFAULT NULL,
  `coatCheck` varchar(12) DEFAULT NULL,
  `corkage` varchar(12) DEFAULT NULL,
  `counterService` varchar(12) DEFAULT NULL,
  `dogsAllowed` varchar(12) DEFAULT NULL,
  `goodForGroups` varchar(12) DEFAULT NULL,
  `goodForKids` varchar(12) DEFAULT NULL,
  `happyHour` varchar(12) DEFAULT NULL,
  `hasTV` varchar(12) DEFAULT NULL,
  `noise` varchar(15) DEFAULT NULL,
  `open24Hrs` varchar(12) DEFAULT NULL,
  `outdoorSeating` varchar(12) DEFAULT NULL,
  `reservations` varchar(12) DEFAULT NULL,
  `smoking` varchar(15) DEFAULT NULL,
  `tableService` varchar(12) DEFAULT NULL,
  `wheelchairAcc` varchar(12) DEFAULT NULL,
  `wifi` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/yelp/business" REPLACE INTO TABLE business;


DROP TABLE IF EXISTS `covid`;


CREATE TABLE `covid` (
  `business_id` varchar(30) NOT NULL,
  `highlights` text DEFAULT NULL,
  `delivery_or_takeout` text DEFAULT NULL,
  `grubhub_enabled` text DEFAULT NULL,
  `call_to_action_enabled` text DEFAULT NULL,
  `request_a_quote_enabled` text DEFAULT NULL,
  `covid_banner` text DEFAULT NULL,
  `temporarily_closed_until` text DEFAULT NULL,
  `virtual_services_offered` text DEFAULT NULL,
  KEY `business_id_index` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/yelp/covid" REPLACE INTO TABLE covid;


DROP TABLE IF EXISTS `review`;


CREATE TABLE `review` (
  `user_id` varchar(30) NOT NULL,
  `funny` smallint(6) DEFAULT NULL,
  `useful` smallint(6) DEFAULT NULL,
  `stars` smallint(6) DEFAULT NULL,
  `business_id` varchar(30) NOT NULL,
  `date` varchar(20) DEFAULT NULL,
  `cool` smallint(6) DEFAULT NULL,
  KEY `user_id_index` (`user_id`),
  KEY `business_id_index` (`business_id`),
  KEY `date_index` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/yelp/review" REPLACE INTO TABLE review;


DROP TABLE IF EXISTS `tip`;


CREATE TABLE `tip` (
  `user_id` varchar(30) NOT NULL,
  `business_id` varchar(30) NOT NULL,
  `date` varchar(20) DEFAULT NULL,
  `compliment_count` smallint(6) DEFAULT NULL,
  KEY `user_id_index` (`user_id`),
  KEY `business_id_index` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/yelp/tip" REPLACE INTO TABLE tip;


DROP TABLE IF EXISTS `user`;


CREATE TABLE `user` (
  `review_count` smallint(5) unsigned DEFAULT NULL,
  `fans` smallint(5) unsigned DEFAULT NULL,
  `cool` int(10) unsigned DEFAULT NULL,
  `compliment_funny` smallint(5) unsigned DEFAULT NULL,
  `compliment_writer` smallint(5) unsigned DEFAULT NULL,
  `yelping_since` varchar(20) DEFAULT NULL,
  `compliment_more` smallint(5) unsigned DEFAULT NULL,
  `compliment_cute` smallint(5) unsigned DEFAULT NULL,
  `compliment_profile` smallint(5) unsigned DEFAULT NULL,
  `funny` int(10) unsigned DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `compliment_note` smallint(5) unsigned DEFAULT NULL,
  `elite` text DEFAULT NULL,
  `compliment_plain` int(10) unsigned DEFAULT NULL,
  `compliment_photos` smallint(5) unsigned DEFAULT NULL,
  `compliment_list` smallint(5) unsigned DEFAULT NULL,
  `compliment_hot` smallint(5) unsigned DEFAULT NULL,
  `useful` int(10) unsigned DEFAULT NULL,
  `user_id` varchar(30) NOT NULL,
  `average_stars` float DEFAULT NULL,
  `compliment_cool` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOAD DATA LOCAL INFILE "mqm/annual/source_files/yelp/user" REPLACE INTO TABLE user;

