CREATE TABLE `airports` (
  `AirportID` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `Country` varchar(80) DEFAULT NULL,
  `IATA` char(3) DEFAULT NULL,
  `ICAO` char(4) DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Altitude` int(11) DEFAULT NULL,
  `UTCOffset` float DEFAULT NULL,
  `DST` char(1) DEFAULT NULL,
  `Timezone` varchar(60) DEFAULT NULL,
  `Type` varchar(10) DEFAULT NULL,
  `Source` char(10) DEFAULT NULL,
  PRIMARY KEY (`AirportID`),
  KEY `iata` (`IATA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

load data infile "mqm/annual/source_files/airline_ontime/airports" replace into table airports;

drop table if exists `ontime`;

CREATE TABLE `ontime` (
  `Year` int(11) DEFAULT NULL,
  `Month` int(11) DEFAULT NULL,
  `DayofMonth` int(11) DEFAULT NULL,
  `DayOfWeek` int(11) DEFAULT NULL,
  `DepTime` int(11) DEFAULT NULL,
  `CRSDepTime` int(11) DEFAULT NULL,
  `ArrTime` int(11) DEFAULT NULL,
  `CRSArrTime` int(11) DEFAULT NULL,
  `UniqueCarrier` varchar(5) DEFAULT NULL,
  `FlightNum` int(11) DEFAULT NULL,
  `TailNum` varchar(8) DEFAULT NULL,
  `ActualElapsedTime` int(11) DEFAULT NULL,
  `CRSElapsedTime` int(11) DEFAULT NULL,
  `AirTime` int(11) DEFAULT NULL,
  `ArrDelay` int(11) DEFAULT NULL,
  `DepDelay` int(11) DEFAULT NULL,
  `Origin` varchar(3) DEFAULT NULL,
  `Dest` varchar(3) DEFAULT NULL,
  `Distance` int(11) DEFAULT NULL,
  `TaxiIn` int(11) DEFAULT NULL,
  `TaxiOut` int(11) DEFAULT NULL,
  `Cancelled` int(11) DEFAULT NULL,
  `CancellationCode` varchar(1) DEFAULT NULL,
  `Diverted` varchar(1) DEFAULT NULL,
  `CarrierDelay` int(11) DEFAULT NULL,
  `WeatherDelay` int(11) DEFAULT NULL,
  `NASDelay` int(11) DEFAULT NULL,
  `SecurityDelay` int(11) DEFAULT NULL,
  `LateAircraftDelay` int(11) DEFAULT NULL,
  KEY `year` (`Year`),
  KEY `date` (`Year`,`Month`,`DayofMonth`),
  KEY `origin` (`Origin`),
  KEY `dest` (`Dest`),
  KEY `uniquecarrier_ind` (`UniqueCarrier`),
  KEY `tailnum_ind` (`TailNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

load data infile "mqm/annual/source_files/airline_ontime/ontime" replace into table ontime;
