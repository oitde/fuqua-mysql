#!/bin/bash
PROG=$(basename $0)
IP=$(hostname -i)
YEAR=$(date +"%Y")
# Create the group jovyan
groupadd -g 10000 jovyan
# Create the group conda
groupadd -g 10001 conda
#
## Creates the user and home directory for jovyan
useradd -u 10000 -g 10000 -d /home/jovyan jovyan
# Adds jovyan to a secondary group,‘conda’
usermod -a -G conda jovyan  

mkdir /home/jovyan/{work,.jupyter}
chown -R jovyan:jovyan /home/jovyan/.jupyter
chown jovyan:conda  /home/jovyan/work
chmod 774  /home/jovyan{,/work}

cat <<EOF >/home/jovyan/.jupyter/jupyter_notebook_config.py
# Configuration file for jupyter-notebook." 
c = get_config()  #noqa
import platform
c.ServerApp.custom_display_url = platform.node() + ":8888"
c.ServerApp.open_browser = False
c.ServerApp.ip = '*'
c.ServerApp.port = 8888
c.IdentityProvider.token = '$student_pw'
c.ServerApp.ip = '$IP'
c.ConnectionFileMixin.ip = '$IP'
c.KernelManager.ip = '$IP'
c.ServerApp.certfile = '/etc/pki/tls/fullchain1.pem'
c.ServerApp.keyfile = '/etc/pki/tls/private/privkey1.pem'
c.ServerApp.base_url = '/work/'
c.ServerApp.file_to_run = '"Welcome to Data Infrastructure ${YEAR}.ipynb"'
EOF
chmod 664 /home/jovyan/.jupyter/jupyter_notebook_config.py
chown jovyan:jovyan /home/jovyan/.jupyter/jupyter_notebook_config.py

cat > /home/jovyan/work/"Welcome to Data Infrastructure ${YEAR}.ipynb" << EOF
{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "MQM ${YEAR}-$((YEAR+1))  \n",
    "Fall Term  \n",
    "Duke University, The Fuqua School of Business  \n",
    "**Data Infrastructure**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to Data Infrastructure!\n",
    "\n",
    "* This Jupyter Notebook is intended to provide you with a unique password that you will use to login to the course database server. You will use the same password to access the databases via Jupyter or MySQL Workbench.\n",
    "* Your unique password is in the cell below..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**$student_pw**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using your password, please try to connect to the server as follows...\n",
    "* First, run the \"cell\" directly below (we will discuss what this code is actually doing later in the course)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pymysql\n",
    "pymysql.install_as_MySQLdb()\n",
    "%reload_ext sql"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Next, enter username **student** and your unique database password provided (above) into the cell below.  For example, if your unique database password is **secret456**, your code in the cell below would be:\n",
    "\n",
    "<h3><center>%sql mysql://student:secret456@localhost/</center></h3>\n",
    "\n",
    "* Input your credentials in the cell below and run the cell. **NOTE: you may get an error the first time you run the cell below. If so, simply run the cell again and the error should go away. This is a slight bug that will not affect your connection.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%sql mysql://student:unique_pw_from_above@localhost/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Finally, let's connect to a specific database and run a short query.  Run the following two cells sequentially.  If your output from running the second cell below is **10**, we're in business!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sql\n",
    "\n",
    "USE lecture2;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%sql\n",
    "\n",
    "SELECT COUNT(*)\n",
    "FROM   Reviews;"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
EOF

cat > /etc/systemd/system/jupyter.service << EOF
[Unit]
Description=Jupyter Notebook

[Service]
Type=simple
PIDFile=/run/jupyter.pid
# Step 1 and Step 2 details are here..
# ------------------------------------
User=jovyan
Group=jovyan
ExecStart=/opt/conda/bin/jupyter-notebook --notebook-dir=/home/jovyan/work/ --ip=${IP} --port=8888 --no-browser --log-level=INFO --ServerApp.allow_password_change=False --ServerApp.allow_origin='*' --ServerApp.allow_remote_access=True 
WorkingDirectory=/home/jovyan/work
Restart=always
RestartSec=10
#KillMode=mixed

[Install]
WantedBy=multi-user.target
EOF

### Certbot
my_cname=$(hostname)
certbot --server https://locksmith.oit.duke.edu/acme/v2/directory --agree-tos --email fuqua-security@duke.edu --no-eff-email -d ${my_cname} --nginx -n
cp -p /etc/letsencrypt/live/${my_cname}/fullchain.pem /etc/pki/tls/fullchain1.pem
cp -p /etc/letsencrypt/live/${my_cname}/privkey.pem /etc/pki/tls/private/privkey1.pem
chown jovyan /etc/pki/tls/fullchain1.pem
chown jovyan /etc/pki/tls/private/privkey1.pem
chmod 660 /etc/pki/tls/private/privkey1.pem

systemctl daemon-reload
systemctl enable --now jupyter.service