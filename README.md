
## Masters of Quantitative Management (MQM)

Every Fall term, the Fuqua School of Business offeres it's Masters of Quantitative Management (MQM) course. Each student is assigned their own Linux VM. The VM has MariaDB installed and pre-loaded with several databses used in the course. Database access is via Jupyter Notebook application.

## VM Installation

The VM are created by the VCM group at OIT. After the OS is installed and the systems configured on the network, a VCM administrator will clone the [fuqua-mysql](https://gitlab.oit.duke.edu/oitde/fuqua-mysql) git repository onto the server, creating a temporary directory for the purpose named `/tmp/<randomstring>`. There must be a shellscript named `install.sh` at the base of the repository, and it is then executed by the VCM admin. 

## Installation script

Tasks performed by the installation script can be broken down into 5 stages.

1. `install.sh`: Administrative tasks. Create a unique key for the student who will use the system, and copy in an annual ssh publickey for remote superuser actions.
1. `kickoff_base_sw.sh`: Install required non-standard software packages. These include:
    - python3.8 python38-devel certbot python3-certbot-nginx gcc kernel-headers-$(uname -r) gzip
    - MariaDB, nginx, Anaconda
    - modules: jupyter-c-kernel, pymysql, ipython-sql
1. `kickoff_dbs.sh`: MariaDB setup.
    - As a one-time effort (unless the coursework databases are changed), the previous generation of code is used to create the databases on any mariadb instance. From there, the table data is extracted using a `SELECT INTO OUTFILE` command, creating a text file for each of the tables. This file is given the name of the table then zipped up and stored in the gilab repository under a directory with the same name as the database. 
    - Configure coursework databases. These are populated by gzip mysql dump files in the repo, using a `LOAD DATA INFILE` command, providing substantial time savings over the previously used method. The databases are:
        - airline_ontime
        - dognitiondb
        - lahman2021
        - lecture2
        - lecture3
        - pir
        - sanford
        - simpsons
        - yelp
    - Create a 'student' account in MariaDB, using the token created in the first stage, and grant it the appropiate access rights.
    - Configure `/etc/my.cnf` and `/root/.my.cnf`.
1. `kickoff_jupyter.sh`: Jupyter notebook setup.
    - Add standard jovyan user and group.
    - Create `jupyter_notebook_config.py` and populate with the student token created in the first stage. This token will server as both jupyter notebook and mariadb access codes.
    - Create the `Welcome to Data Infrastructure <year>` notebook. This provides the student with the login code and is displayed when connecting to port 8888.
    - Cretae a systemd unit-file for jupyter, and start the service.
    - Register an http certificate with [certbot](https://locksmith.oit.duke.edu).
1. `install.sh`: Clean up, deleting all of the install code.

## Remote management

At various times during the term, database access needs to be revoked and services stopped, for quizzes and final exam; and there are databases that are enabled after fixed periods. There are some ansible playbooks on frosty (fsb-2641cd25.fuqua.duke.edu) in /opt/devops/infra-ansible/infra.collection/playbooks directory:

- `mqm_postinstall.yml`: This playbook performs a few basic operations for each entry in the mqm2024.ini inventory file:
    - Scrape the jupyter token from VM host *inventory_hostname*.
    - Extract the netid from ansible variable *inventory_hostname*.
    - Annotate the initial inventory file with net_id and token values.
    - Insert (netid, host_name, jupyter_token) row into `mysql://mqm-db-node-01/mqm_class_2024/student_info`.
    - Generate a new SSL certificate with certbot:
        - In the initial install phase, there is no mapping from FQDN hostname to the netid-constructed CNAME. This process updates the certificate to include both.
- `mqm_db_permissions.yml`: Accepts a single extra variables, `db_task`, which takes one of several values:
    - grant_dog: "Student access to dognitiondb enabled"
    - grant_lahman: "Student access to lahman2021 enabled"
    - revoke_lahman: "Student access to lahman2021 disabled"
    - gen_log_on: "General logging ON"
    - gen_log_off: "General logging OFF"
    - grant_early: "Kickoff Student DB Permissions"
- `service_onoffon.yml`: Accepts two required variables
    - my_services: a list of one or more systemd service names
    - my_state: started/stopped/restarted

These playbooks can be run from frosty. Examples:
- ansible-playbook -i mqm2024.ini service_onoffon.yml -e "my_services=['jupyter','mariadb']" -e my_state=stopped
- ansible-playbook -i mqm2024.ini -l section_a mqm_db_perms.yml -e db_task=grant_lahman
- ansible-playbook --key-file /root/.ssh/mqmkey  -i mqm2024.ini mqm_postinstall.yml --vault-id mysql@/opt/devops/infa-ansible/infra.collection/secrets/mqm_db

## Configuration for a new academic year

A one-off script, `/opt/devops/infra-ansible/infra.collection/utility/mqm_hosts_bootstrap.sh` creates a simple inventory containing only the vm names derived from the spreadsheet supplied by Academic Affairs. This is a simple textual construction, essentially `printf 'fuqua-%s.colab.duke.edu', netID`. The `mqm_postinstall.yml` playbook then annotates the file to provide the final inventory file with the following structure:

    [section_a]
    vcm-ta204.vm.duke.edu net_id=ta204 token=123456789
    vcm-nc249.vm.duke.edu net_id=nc249 token=123456789
    vcm-bad49.vm.duke.edu net_id=bad49 token=123456789
    vcm-mx62.vm.duke.edu net_id=mx62 token=123456789

    [section_b]
    vcm-hz305.vm.duke.edu net_id=hz305 token=123456789
    vcm-pb231.vm.duke.edu net_id=pb231 token=123456789
    vcm-gjc28.vm.duke.edu net_id=gjc28 token=123456789
    vcm-hh292.vm.duke.edu net_id=hh292 token=123456789

    [all:children]
    section_a
    section_b

### Prerequisite for student access to jupyter
On the MQM database host, currently `mqm-db-node-01.fuqua.duke.edu`, connect to the mariadb instance and:
- create the mqm_class_*year* database
- create the mqm_class_*year*.student_info table, consisting of the following row:
#

    +---------------+-------------+------+-----+---------+-------+
    | Field         | Type        | Null | Key | Default | Extra |
    +---------------+-------------+------+-----+---------+-------+
    | netid         | varchar(10) | NO   | PRI | NULL    |       |
    | jupyter_token | varchar(13) | NO   |     | NULL    |       |
    | host_name     | varchar(50) | YES  |     | NULL    |       |
    +---------------+-------------+------+-----+---------+-------+


## Contents of gitlab repository
Official location: `https://gitlab.oit.duke.edu/oitde/fuqua-mysql`

    .
    ├── README.md
    ├── install.sh
    ├── kickoff_base_sw.sh
    ├── kickoff_dbs.sh
    ├── kickoff_jupyter.sh
    └── mqm
        └── annual
            └── source_files
                ├── airline_ontime
                │   ├── airports.gz
                │   └── ontime.gz
                ├── build.airline_ontime.sql
                ├── build.dognitiondb.sql
                ├── build.lahman2021.sql
                ├── build.lecture2.sql
                ├── build.lecture3.sql
                ├── build.pir.sql
                ├── build.sanford.sql
                ├── build.simpsons.sql
                ├── build.yelp.sql
                ├── dognitiondb
                │   ├── complete_tests.gz
                │   ├── dogs.gz
                │   ├── exam_answers.gz
                │   ├── reviews.gz
                │   ├── site_activities.gz
                │   └── users.gz
                ├── lahman2021
                │   ├── AllstarFull.gz
                │   ├── Appearances.gz
                │   ├── AwardsManagers.gz
                │   ├── AwardsPlayers.gz
                │   ├── AwardsShareManagers.gz
                │   ├── AwardsSharePlayers.gz
                │   ├── Batting.gz
                │   ├── BattingPost.gz
                │   ├── CollegePlaying.gz
                │   ├── Fielding.gz
                │   ├── FieldingOF.gz
                │   ├── FieldingOFsplit.gz
                │   ├── FieldingPost.gz
                │   ├── HallOfFame.gz
                │   ├── HomeGames.gz
                │   ├── Managers.gz
                │   ├── ManagersHalf.gz
                │   ├── Parks.gz
                │   ├── People.gz
                │   ├── Pitching.gz
                │   ├── PitchingPost.gz
                │   ├── Salaries.gz
                │   ├── Schools.gz
                │   ├── SeriesPost.gz
                │   ├── Teams.gz
                │   ├── TeamsFranchises.gz
                │   └── TeamsHalf.gz
                ├── lecture2
                │   └── Reviews.gz
                ├── lecture3
                │   ├── CTE_Examp.gz
                │   ├── Dogs.gz
                │   └── Owners.gz
                ├── mqmkey.pub
                ├── my.cnf-LOAD
                ├── pir
                │   └── five_ep.gz
                ├── sanford
                │   └── health.gz
                ├── simpsons
                │   ├── characters.gz
                │   ├── episodes.gz
                │   ├── locations.gz
                │   └── script_lines.gz
                └── yelp
                    ├── business.gz
                    ├── covid.gz
                    ├── review.gz
                    ├── tip.gz
                    └── user.gz

    13 directories, 66 files
